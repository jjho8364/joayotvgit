package com.zaffran.joayotv.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.zaffran.joayotv.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class MaruEpsListActivity extends Activity implements View.OnClickListener, LocationListener {
    private String TAG = " MaruEpsListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;
    private ListView listView;
    private LinearLayout youtubeAd;

    private int adsCnt = 0;
    private String listUrl = "";

    private String nextUrl = "";
    private String nextTitle = "";
    private String nextImgUrl = "";
    private GetIframe getIframe;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";
    private boolean getLastFlag = true;

    List<String> listTitleArr;
    List<String> listImgUrlArr;
    List<String> listPageUrlArr;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maru_eps_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);
        tv_title.setText(title);

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        youtubeAd = (LinearLayout)findViewById(R.id.youtubead);
        youtubeAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "clicked youtube");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCDWC_3sE9EuVHTDI-OZEXgw"));
                startActivity(intent);
            }
        });

        listView = (ListView)findViewById(R.id.list_listview);

        //// paging ////
        tv_currentPage = (TextView)findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getListView = new GetListView();
        getListView.execute();

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listTitleArr = new ArrayList<String>();
            listImgUrlArr = new ArrayList<String>();
            listPageUrlArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(MaruEpsListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                if(lastPage.equals("1")){
                    Log.d(TAG, "baseUrl : " + baseUrl);
                    doc = Jsoup.connect(baseUrl).timeout(15000).get();
                } else {
                    Log.d(TAG, "baseUrl : " + baseUrl + pageNum);
                    doc = Jsoup.connect(baseUrl + pageNum).timeout(15000).get();
                }

                Elements elements = doc.select(".entry_list .each-video");

                for(Element element: elements) {

                    String title = element.select(".item-thumbnail a").attr("title");
                    String imgUrl = element.select(".item-thumbnail img").attr("src");
                    String listUrl = element.select(".item-thumbnail a").attr("href");

                    listTitleArr.add(title);
                    listImgUrlArr.add(imgUrl);
                    listPageUrlArr.add(listUrl);
                }

                ////////////// get lat page /////////////
                if(lastPage.equals("1") && getLastFlag){
                    Elements btns = doc.select(".pages a");
                    if(btns.size() == 0) {	//page 1
                        lastPage = "1";
                    } else if(btns.size() < 6){
                        lastPage = btns.get(btns.size()-2).text();
                        String getUrl = btns.get(btns.size()-2).attr("id");
                        String[] arrUrl = getUrl.split("/");
                        baseUrl = "";
                        for(int i=0 ; i<arrUrl.length-1 ; i++) {
                            baseUrl += arrUrl[i] + "/";
                        }
                    } else { // page 2
                        String[] lastPageUrl = btns.get(btns.size()-1).attr("id").split("/");
                        lastPage = lastPageUrl[lastPageUrl.length-1];
                        String getUrl = btns.get(btns.size()-2).attr("id");
                        String[] arrUrl = getUrl.split("/");
                        baseUrl = "";
                        for(int i=0 ; i<arrUrl.length-1 ; i++) {
                            baseUrl += arrUrl[i] + "/";
                        }
                        Log.d(TAG, "refresh baseUrl : " + baseUrl);
                        getLastFlag = false;
                        //System.out.println(getUrl);
                    }
                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //// paging ////
            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText(lastPage);

            if(MaruEpsListActivity.this != null && listTitleArr.size() != 0){
                /*if(adapter == null){
                    adapter = new ArrayAdapter<String>(MaruEpsListActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
                } else {
                    Log.d(TAG, "33");
                    adapter.clear();
                    adapter = new ArrayAdapter<String>(MaruEpsListActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
                    adapter.notifyDataSetChanged();
                }*/

                adapter = new ArrayAdapter<String>(MaruEpsListActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        adsCnt++;

                        nextUrl = listPageUrlArr.get(position);
                        nextTitle = listTitleArr.get(position);
                        nextImgUrl = listImgUrlArr.get(position);

                        getIframe = new GetIframe();
                        getIframe.execute();
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    public class GetIframe extends AsyncTask<Void, Void, Void> {

        String iframeUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(MaruEpsListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                Log.d(TAG, "nextUrl : " + nextUrl);
                doc = Jsoup.connect(nextUrl).timeout(20000).get();

                iframeUrl = doc.select(".iframetrack iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(this != null){
                Intent intent = new Intent(MaruEpsListActivity.this, MaruListActivity.class);
                intent.putExtra("listUrl", iframeUrl);
                intent.putExtra("title", nextTitle);
                intent.putExtra("imgUrl", nextImgUrl);
                intent.putExtra("adsCnt", "0");
                startActivity(intent);
            }

            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(MaruEpsListActivity.this, MaruEpsListActivity.class);
        intent.putExtra("listUrl", listUrl);
        intent.putExtra("title", title);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("adsCnt", ""+adsCnt);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getIframe != null){
            getIframe.cancel(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
        }
    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
