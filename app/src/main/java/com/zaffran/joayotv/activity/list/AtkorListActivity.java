package com.zaffran.joayotv.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.zaffran.joayotv.R;
import com.zaffran.joayotv.activity.VideoPlayerActivity;
import com.zaffran.joayotv.adapter.MidBtnAdapter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class AtkorListActivity extends Activity {
    private String TAG = " AtkorListActivity - ";
    private ProgressDialog mProgressDialog;
    //ListView listView;
    private GetListView getListView = null;
    private GetPlayer getPlayer = null;
    private String baseUrl = "";

    private ImageView posterView;
    private TextView titleView;
    private TextView storyView;
    private String playerUrl = "";
    private String nextUrl = "";

    private ListView btnListView;

    private int adsCnt = 0;
    private String firstUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mubi_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        firstUrl = intent.getStringExtra("firstUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        posterView = (ImageView)findViewById(R.id.iv_list_poster);
        titleView = (TextView)findViewById(R.id.tv_list_title);
        storyView = (TextView)findViewById(R.id.tv_movie_story);

        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getListView = new GetListView();
        getListView.execute();

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String title = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        List<String> listTitleArr = new ArrayList<String>();
        List<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(AtkorListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                //////////////// episode contents ////////////
                imgUrl = doc.select(".view-content img").attr("src");
                title = doc.select("article h1").text();
                story = doc.select(".view-content p").text();

                /////////////// video button //////////////
                Elements elements = doc.select(".view-content span a");

                for(Element element: elements) {
                    String btnTitle = element.text();
                    String btnVideoUrl = element.attr("href");

                    if(btnVideoUrl.contains("movie.daum.net")) continue;

                    btnTextArr.add(btnTitle);
                    btnVideoUrlArr.add(btnVideoUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            titleView.setText(title);
            storyView.setText(story);
            if(imgUrl != null && !imgUrl.equals("")){
                Picasso.with(AtkorListActivity.this).load(imgUrl).into(posterView);
            } else {
                posterView.setImageResource(R.drawable.noimage);
            }

            ///////// set button list ////////////
            btnListView.setAdapter(new MidBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);

                    getPlayer = new GetPlayer();
                    getPlayer.execute();
                }
            });

            mProgressDialog.dismiss();
        }
    }


    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            playerUrl = "";

            mProgressDialog = new ProgressDialog(AtkorListActivity.this);
            mProgressDialog.setTitle("플레이어를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();

                playerUrl = doc.select("iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "playerUrl : " + playerUrl);
            Intent intent = new Intent(AtkorListActivity.this, VideoPlayerActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

    public String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("http")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }

}
