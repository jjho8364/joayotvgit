package com.zaffran.joayotv.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.squareup.picasso.Picasso;
import com.zaffran.joayotv.R;
import com.zaffran.joayotv.fragment.FakeFragment;
import com.zaffran.joayotv.fragment.FragmentAtkorMid;
import com.zaffran.joayotv.fragment.FragmentAtkorMu;
import com.zaffran.joayotv.fragment.FragmentBayIld;
import com.zaffran.joayotv.fragment.FragmentKorMid;
import com.zaffran.joayotv.fragment.FragmentKorMubi;
import com.zaffran.joayotv.fragment.FragmentLinkMid;
import com.zaffran.joayotv.fragment.FragmentLive;
import com.zaffran.joayotv.fragment.FragmentMaru;
import com.zaffran.joayotv.fragment.FragmentMaruSearch;
import com.zaffran.joayotv.fragment.FragmentQoo;

import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_MARU_DRAMA = 26;
    public final static int FRAGMENT_MARU_ENTER = 27;
    public final static int FRAGMENT_MARU_SEARCH = 28;
    public final static int FRAGMENT_BAY_MUBI = 29;
    public final static int FRAGMENT_BAY_ILD = 30;
    public final static int FRAGMENT_LINK_MID = 31;
    public final static int FRAGMENT_LINK_JOONGD = 32;
    public final static int FRAGMENT_ATKOR_MUBI_HAN_CLASSIC = 33;
    public final static int FRAGMENT_ATKOR_MUBI_OVER_CLASSIC = 6;
    public final static int FRAGMENT_ATKOR_MID = 34;
    public final static int FRAGMENT_KOR_MUBI_HAN = 35;
    public final static int FRAGMENT_KOR_MUBI_OVER = 36;
    public final static int FRAGMENT_KOR_MID = 37;
    public final static int FRAGMENT_KOR_CD = 38;
    public final static int FRAGMENT_KOR_JD = 39;
    public final static int FRAGMENT_ATKOR_JOONGD = 40;
    public final static int FRAGMENT_ATKOR_ILD = 41;
    public final static int FRAGMENT_LIVE = 42;
    public final static int FRAGMENT_LIVE_ENTER = 43;
    public final static int FRAGMENT_FAKE_MENU = 45;

    private TextView tv_fragment01;
    private TextView tv_fragment02;
    private TextView tv_fragment03;
    private TextView tv_fragment04;
    private TextView tv_fragmentMaruDrama;
    private TextView tv_fragmentMaruEnter;
    private TextView tv_fragmentMaruSearh;
    private TextView tv_fragmentBayMubi;
    private TextView tv_fragmentBayIld;
    private TextView tv_fragmentLinkMid;
    private TextView tv_fragmentLinkJoongd;
    private TextView tv_fragmentAtkorMubiHanClassic;
    private TextView tv_fragmentAtkorMubiOverClassic;
    private TextView tv_fragmentAtkorMid;
    private TextView tv_fragmentKorMubiHan;
    private TextView tv_fragmentKorMubiOver;
    private TextView tv_fragmentKorMid;
    private TextView tv_fragmentKorCd;
    private TextView tv_fragmentKorJd;
    private TextView tv_fragmentAtkorJoongd;
    private TextView tv_fragmentAtkorIld;
    private TextView tv_fragmentLive;
    private TextView tv_fragmentLiveEnter;
    private TextView tv_fragmentFakeMenu;

    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragmentMaruDrama;
    private String fragmentMaruEnter;
    private String fragmentMaruSearch;
    private String fragmentBayMubi;
    private String fragmentBayIld;
    private String fragmentLinkMid;
    private String fragmentLinkJoongd;
    private String fragmentAtkorMubiHanClassic;
    private String fragmentAtkorMubiOverClassic;
    private String fragmentAtkorMid;
    private String fragmentKorMubiHan;
    private String fragmentKorMubiOver;
    private String fragmentKorMid;
    private String fragmentKorCd;
    private String fragmentKorJd;
    private String fragmentAtkorJoongd;
    private String fragmentAtkorIld;
    private String fragmentLive;
    private String fragmentLiveEnter;
    private String fragmentFakeMenu;

    private String nextAppUrl = "";
    private String mxPlayerUrl = "";
    private Date curDate;
    private String appStatus = "99";

    private int adsCnt = 0;

    private int adn = 0;

    Date d1;
    Date d2;

    Date installedDate;

    // native ads
    private Button refresh;
    //private UnifiedNativeAd nativeAd;

    // alert dialog
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    View layout;
    TextView finishApp;
    TextView finishAppCancel;

    // fk ads
    private WebView webView;
    private String fkAdsUrl = "";

    private String tistoryUrl = "";
    private String tistoryRndPage = "";

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.custom_dialog, null);

        refresh = layout.findViewById(R.id.btn_refresh);
        finishApp = layout.findViewById(R.id.tv_finish_app);
        finishAppCancel = layout.findViewById(R.id.tv_finish_app_cancel);
        refresh.setOnClickListener(this);
        finishApp.setOnClickListener(this);
        finishAppCancel.setOnClickListener(this);

        // alert dialog
        builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialog = builder.create();

        Intent mainIntent = getIntent();
        appStatus = mainIntent.getStringExtra("appStatus");
        Log.d(TAG, "appStatus : " + appStatus);

        tistoryUrl = mainIntent.getStringExtra("tistoryUrl");
        tistoryRndPage = mainIntent.getStringExtra("tistoryRndPage");

        d1 = new Date();

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        long installedTime = pref.getLong("installedTime",0);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4") && !appStatus.equals("99"))  appStatus = "1";

        //appStatus = "99";

        if(appStatus.equals("99")){
            setContentView(R.layout.activity_main);

            /*HorizontalScrollView horview01 = (HorizontalScrollView)findViewById(R.id.horview01);
            HorizontalScrollView horview02 = (HorizontalScrollView)findViewById(R.id.horview02);
            horview01.setVisibility(View.GONE);
            horview02.setVisibility(View.GONE);*/
            HorizontalScrollView horview01 = (HorizontalScrollView)findViewById(R.id.horview01);
            HorizontalScrollView horview02 = (HorizontalScrollView)findViewById(R.id.horview02);
            HorizontalScrollView horview03 = (HorizontalScrollView)findViewById(R.id.horview03);
            HorizontalScrollView horview04 = (HorizontalScrollView)findViewById(R.id.horview04);
            horview01.setVisibility(View.GONE);
            horview02.setVisibility(View.GONE);
            horview03.setVisibility(View.GONE);
            horview04.setVisibility(View.GONE);

            tv_fragmentFakeMenu = (TextView)findViewById(R.id.tv_fake_menu);

            mCurrentFragmentIndex = FRAGMENT_FAKE_MENU;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.

            editor.commit(); //완료한다.

        } else if (appStatus.equals("1")){

            setContentView(R.layout.activity_main);

            curDate = new Date();

            HorizontalScrollView horview05 = (HorizontalScrollView)findViewById(R.id.horview05);
            horview05.setVisibility(View.GONE);

            tv_fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            tv_fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            tv_fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            tv_fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            tv_fragmentMaruDrama = (TextView)findViewById(R.id.tv_frag_maru_drama);
            tv_fragmentMaruEnter = (TextView)findViewById(R.id.tv_frag_maru_enter);
            tv_fragmentMaruSearh = (TextView)findViewById(R.id.tv_frag_maru_search);
            tv_fragmentBayMubi = (TextView)findViewById(R.id.tv_fragment_bay_mubi);
            tv_fragmentBayIld = (TextView)findViewById(R.id.tv_fragment_bay_ild);
            tv_fragmentLinkMid = (TextView)findViewById(R.id.tv_frag_link_mid);
            tv_fragmentLinkJoongd = (TextView)findViewById(R.id.tv_frag_link_joongd);
            tv_fragmentAtkorMubiHanClassic = (TextView)findViewById(R.id.tv_frag_atkor_mubi_han_classic);
            tv_fragmentAtkorMubiOverClassic = (TextView)findViewById(R.id.tv_frag_atkor_mubi_over_classic);
            tv_fragmentAtkorMid = (TextView)findViewById(R.id.tv_frag_atkor_mid);
            tv_fragmentAtkorJoongd = (TextView)findViewById(R.id.tv_frag_atkor_joongd);
            tv_fragmentAtkorIld = (TextView)findViewById(R.id.tv_frag_atkor_ild);
            tv_fragmentKorMubiHan = (TextView)findViewById(R.id.tv_frag_kor_mubi_han);
            tv_fragmentKorMubiOver = (TextView)findViewById(R.id.tv_frag_kor_mubi_over);
            tv_fragmentKorMid = (TextView)findViewById(R.id.tv_frag_kor_mid);
            tv_fragmentKorCd = (TextView)findViewById(R.id.tv_frag_kor_cd);
            tv_fragmentKorJd = (TextView)findViewById(R.id.tv_frag_kor_jd);
            tv_fragmentLive = (TextView)findViewById(R.id.tv_frag_livetv);
            tv_fragmentLiveEnter = (TextView)findViewById(R.id.tv_frag_livetv_enter);
            //fragmentFakeMenu = (TextView)findViewById(R.id.tv_fake_menu);

            tv_fragment01.setOnClickListener(this);
            tv_fragment02.setOnClickListener(this);
            tv_fragment03.setOnClickListener(this);
            tv_fragment04.setOnClickListener(this);
            tv_fragmentMaruDrama.setOnClickListener(this);
            tv_fragmentMaruEnter.setOnClickListener(this);
            tv_fragmentMaruSearh.setOnClickListener(this);
            tv_fragmentBayMubi.setOnClickListener(this);
            tv_fragmentBayIld.setOnClickListener(this);
            tv_fragmentLinkMid.setOnClickListener(this);
            tv_fragmentLinkJoongd.setOnClickListener(this);
            tv_fragmentAtkorMubiHanClassic.setOnClickListener(this);
            tv_fragmentAtkorMubiOverClassic.setOnClickListener(this);
            tv_fragmentAtkorMid.setOnClickListener(this);
            tv_fragmentAtkorJoongd.setOnClickListener(this);
            tv_fragmentAtkorIld.setOnClickListener(this);
            tv_fragmentKorMubiHan.setOnClickListener(this);
            tv_fragmentKorMubiOver.setOnClickListener(this);
            tv_fragmentKorMid.setOnClickListener(this);
            tv_fragmentKorCd.setOnClickListener(this);
            tv_fragmentKorJd.setOnClickListener(this);
            tv_fragmentLive.setOnClickListener(this);
            tv_fragmentLiveEnter.setOnClickListener(this);

            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            fragmentMaruDrama = mainIntent.getStringExtra("fragMaruDrama2");
            fragmentMaruEnter = mainIntent.getStringExtra("fragMaruEnter2");
            fragmentMaruSearch = mainIntent.getStringExtra("fragMaruSearch");
            fragmentBayMubi = mainIntent.getStringExtra("fragBayMubi");
            fragmentBayIld = mainIntent.getStringExtra("fragBayIld");
            fragmentLinkMid = mainIntent.getStringExtra("fragLinkMid");
            fragmentLinkJoongd = mainIntent.getStringExtra("fragLinkJoongd");
            fragmentAtkorMubiHanClassic = mainIntent.getStringExtra("fragAtkorMubiHanClassic");
            fragmentAtkorMubiOverClassic = mainIntent.getStringExtra("fragAtkorMubiOverClassic");
            fragmentAtkorMid = mainIntent.getStringExtra("fragAtkorMid");
            fragmentAtkorJoongd = mainIntent.getStringExtra("fragAtkorJoongd");
            fragmentAtkorIld = mainIntent.getStringExtra("fragAtkorIld");
            fragmentKorMubiHan = mainIntent.getStringExtra("fragKorMubiHan");
            fragmentKorMubiOver = mainIntent.getStringExtra("fragKorMubiOver");
            fragmentKorMid = mainIntent.getStringExtra("fragKorMid");
            fragmentKorCd = mainIntent.getStringExtra("fragKorCd");
            fragmentKorJd = mainIntent.getStringExtra("fragKorJd");
            fragmentLive = mainIntent.getStringExtra("fragLiveTv");
            fragmentLiveEnter = mainIntent.getStringExtra("fragLiveTvEnter");

            mCurrentFragmentIndex = FRAGMENT_MARU_SEARCH;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.

            editor.commit(); //완료한다.

            // live tv channel
            if(installedTime == 0){
                installedTime = new Date().getTime();
                Log.d(TAG, "first install : " + installedTime);
                editor.putLong("installedTime", installedTime); //First라는 key값으로 id 데이터를 저장한다.
            } else {
                long nowTime = new Date().getTime();
                long diff = nowTime - installedTime;
                long min = diff/1000/60 ;
                long hour = min/60;
                Log.d(TAG, "now - istalled time : " + hour);
                if(hour >= 12) {
                    // fake ads is here
                    tv_fragmentLive.setVisibility(View.VISIBLE);
                    tv_fragmentLiveEnter.setVisibility(View.VISIBLE);
                }
            }
        } else if(appStatus.equals("2")){
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")) {   // mx player


        } else if(appStatus.equals("9")) {   // mx player
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closedImgUrl");
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            Log.d(TAG, "  " + nextAppUrl);
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            Picasso.with(this).load(closedImgUrl).into(imgView);
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                    marketLaunch1.setData(Uri.parse(nextAppUrl));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextAppUrl));
                    startActivity(intent);
                }
            });
        }

        // admob
        MobileAds.initialize(this, getResources().getString(R.string.app_id));
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment01:
                Log.d(TAG, "getID : " + v.getId());
                offColorTv();
                tv_fragment01.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02:
                Log.d(TAG, "getID : " + v.getId());
                offColorTv();
                tv_fragment02.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                tv_fragment03.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                tv_fragment04.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_drama:
                Log.d(TAG, "getID : " + v.getId());
                offColorTv();
                tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_DRAMA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_enter:
                offColorTv();
                tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_search:
                offColorTv();
                tv_fragmentMaruSearh.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_SEARCH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_bay_mubi:
                offColorTv();
                tv_fragmentBayMubi.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_MUBI;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_bay_ild:
                offColorTv();
                tv_fragmentBayIld.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_ILD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_link_mid:
                offColorTv();
                tv_fragmentLinkMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_link_joongd:
                offColorTv();
                tv_fragmentLinkJoongd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_JOONGD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mubi_han_classic:
                offColorTv();
                tv_fragmentAtkorMubiHanClassic.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MUBI_HAN_CLASSIC;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mubi_over_classic:
                offColorTv();
                tv_fragmentAtkorMubiOverClassic.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MUBI_OVER_CLASSIC;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mid:
                offColorTv();
                tv_fragmentAtkorMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_joongd:
                offColorTv();
                tv_fragmentAtkorJoongd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_JOONGD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_ild:
                offColorTv();
                tv_fragmentAtkorIld.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_ILD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mubi_han:
                offColorTv();
                tv_fragmentKorMubiHan.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MUBI_HAN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mubi_over:
                offColorTv();
                tv_fragmentKorMubiOver.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MUBI_OVER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mid:
                offColorTv();
                tv_fragmentKorMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_cd:
                offColorTv();
                tv_fragmentKorCd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_CD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_jd:
                offColorTv();
                tv_fragmentKorJd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_JD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_livetv:
                offColorTv();
                tv_fragmentLive.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_livetv_enter:
                offColorTv();
                tv_fragmentLiveEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LIVE_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.btn_closed:
                Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                Log.d(TAG, "  " + nextAppUrl);
                marketLaunch1.setData(Uri.parse(nextAppUrl));
                startActivity(marketLaunch1);
                break;
            case R.id.btn_refresh :
                //refreshAd();
                break;
            case R.id.tv_finish_app :
                finish();
                break;
            case R.id.tv_finish_app_cancel :
                if(alertDialog != null) alertDialog.cancel();
                break;

        }
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);

        switch (idx) {

            case FRAGMENT_ONE:
                newFragment = new FragmentQoo();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new FragmentQoo();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new FragmentQoo();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new FragmentQoo();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_DRAMA:
                newFragment = new FragmentMaru();
                args.putString("baseUrl", fragmentMaruDrama);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_ENTER:
                newFragment = new FragmentMaru();
                args.putString("baseUrl", fragmentMaruEnter);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_SEARCH:
                newFragment = new FragmentMaruSearch();
                args.putString("baseUrl", fragmentMaruSearch);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_MUBI:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentBayMubi);
                args.putString("type", "%7C4/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_ILD:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentBayIld);
                args.putString("type", "%7C7/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_MID:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkMid);
                args.putString("type", "%7C6/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_JOONGD:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkJoongd);
                args.putString("type", "%7C8/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MUBI_HAN_CLASSIC:
                newFragment = new FragmentAtkorMu();
                args.putString("baseUrl", fragmentAtkorMubiHanClassic);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MUBI_OVER_CLASSIC:
                newFragment = new FragmentAtkorMu();
                args.putString("baseUrl", fragmentAtkorMubiOverClassic);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MID:
                newFragment = new FragmentAtkorMid();
                args.putString("baseUrl", fragmentAtkorMid);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_JOONGD:
                newFragment = new FragmentAtkorMid();
                args.putString("baseUrl", fragmentAtkorJoongd);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_ILD:
                newFragment = new FragmentAtkorMid();
                args.putString("baseUrl", fragmentAtkorIld);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MUBI_HAN:
                newFragment = new FragmentKorMubi();
                args.putString("baseUrl", fragmentKorMubiHan);
                args.putString("searchUrl", "&bo_table=kmovie&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MUBI_OVER:
                newFragment = new FragmentKorMubi();
                args.putString("baseUrl", fragmentKorMubiOver);
                args.putString("searchUrl", "&bo_table=engmovie&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MID:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorMid);
                args.putString("searchUrl", "&bo_table=mid&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_CD:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorCd);
                args.putString("searchUrl", "&bo_table=cd&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_JD:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorJd);
                args.putString("searchUrl", "&bo_table=jd&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLive);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE_ENTER:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLiveEnter);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_MENU:
                newFragment = new FakeFragment();
                newFragment.setArguments(args);
                break;
            default:
                Log.d(TAG, "Unhandle case");
                newFragment = new FragmentMaruSearch();
                args.putString("baseUrl", fragmentMaruSearch);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        tv_fragment01.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment02.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment03.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment04.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruSearh.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentBayMubi.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentBayIld.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkJoongd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMubiHanClassic.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMubiOverClassic.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorJoongd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorIld.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMubiHan.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMubiOver.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorCd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorJd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLive.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLiveEnter.setBackgroundResource(R.drawable.fragment_borther);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "main activity onResume, appStatus : " + appStatus);

        if((appStatus.equals("1")||appStatus.equals("99")) && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            Log.d(TAG, "min : " + min);
            if(min >= 30){
                d1 = new Date();

                SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
                SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.
                editor.commit(); //완료한다.

                adFull();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(alertDialog != null) alertDialog.cancel();
        //refreshAd();
        //alertDialog.show();
    }

    public void adFull(){

    }

    // Back Key가 눌러졌을 때, CloseAd 호출
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 앱을 처음 설치하여 실행할 때,
            //
            // 필요한 리소스를 다운받았는지 여부.
            /*if (mCloseAd.isModuleLoaded()) {
                mCloseAd.show(this);
            } else {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
            }*/

            // native trying
            AdLoader.Builder builder = new AdLoader.Builder( this, getResources().getString(R.string.native_main));

            builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    TemplateView template = layout.findViewById(R.id.my_template);
                    template.setNativeAd(unifiedNativeAd);
                }
            });

            AdLoader adLoader = builder.build();
            adLoader.loadAd(new AdRequest.Builder().build());

            alertDialog.show();


            //showDefaultClosePopup();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup() {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }
}
