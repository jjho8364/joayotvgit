package com.zaffran.joayotv.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.zaffran.joayotv.R;
import com.zaffran.joayotv.activity.VideoPlayerActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class AtkorEpsActivity extends Activity implements View.OnClickListener, LocationListener {
    private String TAG = " AtkorEpsActivity - ";
    private ProgressDialog mProgressDialog;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;
    private ListView listView;

    private int adsCnt = 0;
    private String listUrl = "";

    private String nextUrl = "";
    private String nextTitle = "";
    private String nextImgUrl = "";
    private GetIframe getIframe;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    List<String> listTitleArr;
    List<String> listImgUrlArr;
    List<String> listPageUrlArr;

    private LinearLayout youtubeAd;

    ArrayAdapter adapter;

    private GetPlayer getPlayer = null;
    private String playerUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maru_eps_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);
        tv_title.setText(title);

        listView = (ListView)findViewById(R.id.list_listview);

        //// paging ////
        tv_currentPage = (TextView)findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        youtubeAd = (LinearLayout)findViewById(R.id.youtubead);
        youtubeAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "clicked youtube");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCDWC_3sE9EuVHTDI-OZEXgw"));
                startActivity(intent);
            }
        });

        getListView = new GetListView();
        getListView.execute();

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listTitleArr = new ArrayList<String>();
            listImgUrlArr = new ArrayList<String>();
            listPageUrlArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(AtkorEpsActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                if(lastPage.equals("1")){
                    Log.d(TAG, "baseUrl : " + baseUrl);
                    doc = Jsoup.connect(baseUrl).timeout(15000).get();
                } else {
                    Log.d(TAG, "baseUrl : " + baseUrl + pageNum);
                    doc = Jsoup.connect(baseUrl + pageNum).timeout(15000).get();
                }

                // get image
                imgUrl = doc.select(".img-tag").attr("src");

                Elements elements = doc.select(".view-content p span a");

                for(Element element: elements) {
                    String title = element.text();
                    String imgUrl = "";
                    String listUrl = element.attr("href");

                    if(listUrl.contains("nband.net")) continue;

                    listTitleArr.add(title);
                    listImgUrlArr.add(imgUrl);
                    listPageUrlArr.add(listUrl);
                }

                ////////////// get lat page /////////////
                if(lastPage.equals("1")){
                    Elements btns = doc.select(".pages a");
                    if(btns.size() == 0) {	//page 1
                        lastPage = "1";
                    } else {
                        lastPage = btns.get(btns.size()-1).attr("href").split("spage=")[1];
                    }
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // set image
            if (imgUrl != null && !imgUrl.equals("")){
                Picasso.with(AtkorEpsActivity.this).load(imgUrl).into(img_poster);
            }

            //// paging ////
            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText(lastPage);

            if(AtkorEpsActivity.this != null && listTitleArr.size() != 0){
                adapter = new ArrayAdapter<String>(AtkorEpsActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        /*
                        Intent intent = new Intent(AtkorEpsActivity.this, AtkorListActivity.class);
                        intent.putExtra("listUrl", listPageUrlArr.get(position));
                        intent.putExtra("firstYrl", "");
                        //intent.putExtra("imgUrl", nextImgUrl);
                        intent.putExtra("adsCnt", "0");
                        startActivity(intent);
                         */

                        nextUrl = listPageUrlArr.get(position);

                        getPlayer = new GetPlayer();
                        getPlayer.execute();

                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    public class GetIframe extends AsyncTask<Void, Void, Void> {

        String iframeUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(AtkorEpsActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                Log.d(TAG, "nextUrl : " + nextUrl);
                doc = Jsoup.connect(nextUrl).timeout(20000).get();

                iframeUrl = doc.select(".iframetrack iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(this != null){
                Intent intent = new Intent(AtkorEpsActivity.this, KorListActivity.class);
                intent.putExtra("listUrl", iframeUrl);
                intent.putExtra("title", nextTitle);
                intent.putExtra("imgUrl", nextImgUrl);
                intent.putExtra("adsCnt", "0");
                startActivity(intent);
            }

            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(AtkorEpsActivity.this, AtkorEpsActivity.class);
        intent.putExtra("listUrl", listUrl);
        intent.putExtra("title", title);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("adsCnt", ""+adsCnt);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getIframe != null){
            getIframe.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
        }
    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            playerUrl = "";

            mProgressDialog = new ProgressDialog(AtkorEpsActivity.this);
            mProgressDialog.setTitle("플레이어를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();

                playerUrl = doc.select("iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "playerUrl : " + playerUrl);
            Intent intent = new Intent(AtkorEpsActivity.this, VideoPlayerActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

    public String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("http")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }
}
