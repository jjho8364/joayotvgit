package com.zaffran.joayotv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.zaffran.joayotv.R;
import com.zaffran.joayotv.activity.list.MaruEpsListActivity;
import com.zaffran.joayotv.adapter.GridDramaAdapter;
import com.zaffran.joayotv.item.GridDramaItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentMaruSearch extends Fragment implements View.OnClickListener {
    private final String TAG = " FragmentMaruSearch - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String keyword1 = "";
    private ArrayList<GridDramaItem> listViewItemArr;

    private EditText editText;
    private Button searchBtn;

    InputMethodManager inputManager;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    ArrayAdapter<String> adapter;

    private int adn = 0;

    private String intentListUrl = "";
    private String intentImgUrl = "";
    private String intentTitle = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maru_search, container, false);

        baseUrl = getArguments().getString("baseUrl");
        adsCnt = getArguments().getInt("adsCnt");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_main));
        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "success to load AD");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d(TAG, "failed to load AD");
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "closed AD");
                interstitialAd.loadAd(new AdRequest.Builder().build());

                Intent intent = new Intent(getActivity(), MaruEpsListActivity.class);
                intent.putExtra("listUrl", intentListUrl);
                intent.putExtra("title", intentTitle);
                intent.putExtra("imgUrl", intentImgUrl);
                intent.putExtra("adsCnt", "0");
                startActivity(intent);

            }
        });

        gridView = (GridView)view.findViewById(R.id.gridview);

        editText = (EditText)view.findViewById(R.id.fr20_edit);
        searchBtn = (Button)view.findViewById(R.id.fr20_searchbtn);
        searchBtn.setOnClickListener(this);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl + keyword1);
                doc = Jsoup.connect(baseUrl + keyword1).timeout(10000).get();

                Elements elements = doc.select(".entry_list .each-video");

                for(Element element: elements) {

                    String title = element.select(".item-thumbnail a").attr("title");
                    String imgUrl = element.select(".item-thumbnail img").attr("src");
                    String listUrl = element.select(".item-thumbnail a").attr("href");
                    String update = element.select("p").text();

                    GridDramaItem gridViewItemList = new GridDramaItem(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(getActivity() != null){
                if(listViewItemArr.size() == 0){
                    keyword1 = "";
                    listViewItemArr.clear();
                    GridDramaAdapter adapter = new GridDramaAdapter(getActivity(), listViewItemArr, R.layout.item_grid_drama);
                    gridView.setAdapter(adapter);
                } else {
                    gridView.setAdapter(new GridDramaAdapter(getActivity(), listViewItemArr, R.layout.item_grid_drama));

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                            if(adsCnt == 1){
                                adsCnt++;
                                SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                                editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                                editor.commit(); //완료한다.

                                intentListUrl = listViewItemArr.get(position).getListUrl();
                                intentImgUrl = listViewItemArr.get(position).getImgUrl();
                                intentTitle = listViewItemArr.get(position).getTitle();

                                interstitialAd.show();

                            } else {
                                intentListUrl = listViewItemArr.get(position).getListUrl();
                                intentImgUrl = listViewItemArr.get(position).getImgUrl();
                                intentTitle = listViewItemArr.get(position).getTitle();

                                Intent intent = new Intent(getActivity(), MaruEpsListActivity.class);
                                intent.putExtra("listUrl", intentListUrl);
                                intent.putExtra("title", intentTitle);
                                intent.putExtra("imgUrl", intentImgUrl);
                                intent.putExtra("adsCnt", "0");
                                startActivity(intent);
                            }
                        }
                    });
                }
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr20_searchbtn :
                if(!editText.getText().toString().equals("")){
                    keyword1 = editText.getText().toString().trim();
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                } else {
                    Toast.makeText(getActivity(), "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

}
