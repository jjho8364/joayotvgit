package com.zaffran.joayotv.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zaffran.joayotv.R;
import com.zaffran.joayotv.item.GridDramaItem;

import java.util.ArrayList;

public class GridDramaAdapter extends BaseAdapter {
    String TAG = "GridViewAdapterTAG";
    Activity context;
    ArrayList<GridDramaItem> gridArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<GridDramaItem> arraylist;

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
        public TextView update;
    }

    public GridDramaAdapter(Activity context, ArrayList<GridDramaItem> gridArr, int layout) {
        this.context = context;
        this.gridArr = gridArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<GridDramaItem>();
        arraylist.addAll(gridArr);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.gridview_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.gridview_title);
            viewHolder.update = (TextView)rowView.findViewById(R.id.gridview_update);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        GridDramaItem data = gridArr.get(position);
        if(data.getImgUrl()==null || data.getImgUrl().equals("")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }
        holder.title.setText(data.getTitle());
        holder.update.setText(data.getUpdate());

        return rowView;
    }

    @Override
    public int getCount() {
        return gridArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}